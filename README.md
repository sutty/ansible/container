Sutty Container
===============

Ansible role for starting Docker containers at
[Sutty](https://sutty.nl/)'s infrastructure.

Since each node runs a single container-service, every container is
addressed at container-name.inventory-hostname.

Role Variables
--------------

| Variable           | Description                               | Default value  |
| ------------------ | ----------------------------------------- | -------------- |
| `name`             | Container name                            | -              |
| `container_name`   | Remote container name                     | `name`         |
| `version`          | Version                                   | -              |
| `env`              | Dict of env variables                     | -              |
| `volumes`          | List of volumes                           | -              |
| `published_ports`  | List of ports                             | -              |
| `state`            | State                                     | `started`      |
| `network`          | Network to attach                         | `sutty`        |
| `monit_email_to`   | Send Monit status to this e-email address | -              |
| `monit_email_from` | Send Monit status from this address       | -              |
| `mmmonit`          | Mmmonit instance address:port             | -              |
| `registry`         | Container registry                        | -              |
| `ipv4_address`     | Static IPv4 address                       | auto-assigned  |
| `ipv6_address`     | Static IPv6 address                       | auto-assigned  |

If you don't need `env`, pass it as `env: {}` because providing
a default value hides undefined variables.  Same for `published_ports:
[]` and `volumes: {}`.

Example Playbook
----------------

This container should be available at borg.anarres.sutty.nl and
borg.athshe.sutty.nl after deployment.

```yaml
---
- hosts:
  - "anarres.sutty.nl"
  - "athshe.sutty.nl"
  strategy: free
  remote_user: root
  tasks:
  - name: "borg container"
    include_role:
      name: "sutty-container"
    vars:
      name: "borg"
      version: "3.16.2"
      registry: "custom.regist.ry/namespace"
      state: "started"
      network: "sutty"
      volumes:
      - "/:/srv/data"
      - "/root/.ssh:/root/.ssh"
      env:
        BORG_PASSPHRASE: "{{ vault.backups }}"
        DEST: "{{ inventory_hostname }}"
        SSH_ALIAS: "backup_storage"
        SSH_SERVER: "backup.stora.ge"
        SSH_USER: "sutty"
        SSH_PORT: "222"
        KEEP: "15"
      published_ports: []
```

License
-------

MIT-Antifa
